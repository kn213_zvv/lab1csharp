using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

abstract class MusIns
{
    public abstract void Sound();
    public abstract void Show();
    public abstract void Desc();
    public abstract void History();
    public abstract void ShowInfo();
}
class Scrypka : MusIns
{
    public override void Sound()
    {
        Console.WriteLine("Звуки скрипки");
    }
    public override void Show()
    {
        Console.WriteLine("Скрипка");
    }
    public override void Desc()
    {
        Console.WriteLine("Скри́пка (від староцерк.-слов. скрыпати — «скрипіти», італ. violino) — струнний музичний смичковий інструмент.");
    }
    public override void History()
    {
        Console.WriteLine("Прототипами скрипки були арабський ребаб і німецька рота, злиття яких і утворило віолу. Форми скрипки сформувались до XVI століття; в цьому столітті і на початку наступного, XVII, творили майстри скрипок сімейства Аматі. Їх інструменти відрізняються прекрасною формою і чудовим матеріалом. Взагалі Італія славилася виробництвом скрипок, серед яких скрипки Страдіварі і Гварнері нині цінуються надзвичайно високо.");
    }
    public override void ShowInfo()
    {
        this.Sound();
        this.Show();
        this.Desc();
        this.History();
    }
}
class Trombon : MusIns
{
    public override void Sound()
    {
        Console.WriteLine("Звуки тромбона");
    }
    public override void Show()
    {
        Console.WriteLine("Тромбон");
    }
    public override void Desc()
    {
        Console.WriteLine("Тромбо́н (італ. trombone, збільшуване від tromba — труба; нім. Posaune) — музичний інструмент сімейства мідних духових.");
    }
    public override void History()
    {
        Console.WriteLine("Тромбон - слово італійського походження (італ. trombe - це труба, trombone - велика труба). Англійська предок тромбона іменувався секбат (сакбат, sackbut) і був досить схожий з сучасним інструментом. Вважається, що за останні п'ятсот років тромбон практично не змінився. Це не зовсім так. Змінювалися розміри інструменту, форми мундштука і розтруба.");
    }
    public override void ShowInfo()
    {
        this.Sound();
        this.Show();
        this.Desc();
        this.History();
    }
}
class Violon : MusIns
{
    public override void Sound()
    {
        Console.WriteLine("Звуки віолончелі");
    }
    public override void Show()
    {
        Console.WriteLine("Віолончель");
    }
    public override void Desc()
    {
        Console.WriteLine("Віолонче́ль, також уживається віолонче́ля та віольонче́ля (італ. violoncello, скор. че́льо cello; фр. violoncelle; англ. cello) — струнний смичковий музичний інструмент, родини скрипкових, басо-тенорового регістру.");
    }
    public override void History()
    {
        Console.WriteLine("Поява віолончелі відноситься до початку XVI століття. Спочатку вона застосовувалася як басовий інструмент для супроводу співу або виконання на інструменті вищого регістру. Існували численні різновиди віолончелі, що відрізнялися один від одного розмірами, кількістю струн, строєм (найчастіше зустрічалася настройка на тон нижче сучасної).");
    }
    public override void ShowInfo()
    {
        this.Sound();
        this.Show();
        this.Desc();
        this.History();
    }
}
class Ukulele : MusIns
{
    public override void Sound()
    {
        Console.WriteLine("Звуки укулеле");
    }
    public override void Show()
    {
        Console.WriteLine("Укулеле");
    }
    public override void Desc()
    {
        Console.WriteLine("Укуле́ле (/ˌjuːkəˈleɪli/ ; з гавайської: ʻukulele [ˈʔukuˈlɛlɛ])  — чотириструнний щипковий музичний інструмент, зазвичай з нейлонними струнами.");
    }
    public override void History()
    {
        Console.WriteLine("Перші прообрази укулеле з'явилися у Європі, у середині 15 століття. У ті роки було добре розвинене виробництво струнних інструментів, але більш складні гітари і мандоліни, були досить коштовними, особливо для бродячих музик. Попит народжує пропозицію, і на ринку почали з'являтися зменшені гітари з усього чотирма струнами — кавакіньо, які стали прообразом укулеле.");
    }
    public override void ShowInfo()
    {
        this.Sound();
        this.Show();
        this.Desc();
        this.History();
    }
}
struct Student
{
    public string FirstName;
    public string LastName;
    public int Age;
    public Student(string firstName, string lastName, int age)
    {
        FirstName = firstName;
        LastName = lastName;
        Age = age;
    }
    public Student(string firstName, string lastName)
    {
        FirstName = firstName;
        LastName = lastName;
        Age = 18;
    }
    public void PrintInfo()
    {
        Console.WriteLine("Прізвище: {0}", LastName);
        Console.WriteLine("Ім'я:     {0}", FirstName);
        Console.WriteLine("Вік:      {0}", Age);
    }
}
class Person
{
    public Person(Person previousPerson)
    {
        Name = previousPerson.Name;
        Age = previousPerson.Age;
    }
    public Person(string name, int age)
    {
        Name = name;
        Age = age;
    }
    public int Age { get; set; }

    public string Name { get; set; }

    public string Details()
    {
        return Name + " is " + Age.ToString();
    }
}
class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;
        Student stud = new Student("Vova", "Zakh", 19);
        Student stud1 = new Student("Denus", "Pidip");
        stud1.PrintInfo();
        stud.PrintInfo();
        Person person1 = new Person("George", 40);
        Person person2 = new Person(person1);
        person1.Age = 39;
        person2.Age = 41;
        person2.Name = "Charles";
        Console.WriteLine(person1.Details());
        Console.WriteLine(person2.Details());
        MusIns[] mas = { new Scrypka(), new Trombon(), new Violon(), new Ukulele() };
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].Sound();
        }
        Console.ReadKey();
    }
}
